import toml
from pathlib import Path

__root__ = Path().resolve()
__version__ = toml.load(__root__/'pyproject.toml')['tool']['poetry']['version']
