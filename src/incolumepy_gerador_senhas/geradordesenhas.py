#!/bin/usr/python
""" Gerar senhas por substituição simples de caracteres """


def geradordesenhas(chave: str)-> str:
    senha = ""
    code = {
        'a': '@', 'b': '1', 'c': '2', 'd': '3', 'e': '4', 'f': '5',
        'r': '#', 's': '%', 'm': '$'
    }
    for letra in chave:
        senha += code.get(letra, letra)
    return senha


if __name__ == "__main__":
    r = geradordesenhas(input("Digite a base de tua senha: "))
    print(r)

